# inotify-rsync

This is used to sync folders and files on Linux using rsync and inotify.

# Prerequisites

This package require the [inotify-tools](https://github.com/rvoicilas/inotify-tools/wiki). 

```bash
tar -xvjf inotify-tools-3.14.tar.gz
cd inotify-tools-3.14
./configure
make
make install
```

## Configurations

1. Create a folder `/etc/evagroup` to store bash file.
2. Modify the location of the `source` and `target` folders in the `file-sync.sh` file.
3. Modify the `inotifywait` [options](https://linux.die.net/man/1/inotifywait) in `file-sync.sh` file accordingly.

## Installation

1. Copy the `inotify-rsync.service` to the OS service/system folder.
2. Grant `chomod +x`  permission on the `inotify-rsync.service` file.
3. Copy the `file-sync.sh` file to `/apps/inotify-rsync` folder.
4. Run `sudo systemctl start inotify-rsync.service` to start the service.

If the sync is to remote server, ssh is require, please make the changes in the file-sync.sh file ( add the option `-e ssh source user@address:target` ).




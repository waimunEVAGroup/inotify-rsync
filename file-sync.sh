#!/bin/bash

while true; do
  inotifywait -r -e modify,attrib,close_write,move,create,delete source
  rsync -avz source/* target 
done